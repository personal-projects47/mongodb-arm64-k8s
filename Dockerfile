FROM ubuntu:groovy-20210416

RUN apt-get update && apt-get install -y locales && rm -rf /var/lib/apt/lists/* \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

RUN  apt-get update \
  && apt-get install -y wget gnupg \
  && rm -rf /var/lib/apt/lists/*

RUN wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | apt-key add -

RUN echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-4.4.list

RUN apt-get update && apt-get -y install mongodb-org-shell mongodb-org-mongos mongodb-org-tools \
    && apt-get download -y mongodb-org-server && dpkg --unpack mongodb-org*.deb && \
    dpkg --configure mongodb-org-server || rm -rf /var/lib/apt/lists/*

EXPOSE 27017
CMD ["/usr/bin/mongod", "--dbpath", "/srv/mongodb/", "--bind_ip_all", "--maxConns", "20"]